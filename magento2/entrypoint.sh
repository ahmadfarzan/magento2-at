if [ -z "${nginx_host}" ] ; then
    nginx_host=http://magento2.local
fi

if [ "${nginx_host}" != "" ] ; then
    sed -i "s@{{host}}@${nginx_host}@g" /etc/nginx/sites-available/default
    sed -i "s@server_name http://@server_name @g" /etc/nginx/sites-available/default
fi

if [ "${nginx_root}" != "" ] ; then
    sed -i "s@{{root}}@${nginx_root}@g" /etc/nginx/sites-available/default
fi

if [ "${mage_mode}" != "" ] ; then
    sed -i "s@{{mode}}@${mage_mode}@g" /etc/nginx/sites-available/default
fi

# PHP7-FPM.
service php7.0-fpm start

nginx_root_size=`du -s ${nginx_root} | cut -f1`

chown www-data:www-data -R ${nginx_root}

if [ "$(($nginx_root_size+0))" -lt 100 ]
then
  wget https://github.com/magento/magento2/archive/2.1.2.tar.gz -O /tmp/magento2.tar.gz
  tar -zxvf /tmp/magento2.tar.gz -C /tmp/
  cp -avr /tmp/magento2-2.1.2/. ${nginx_root}
  rm /tmp/magento2.tar.gz
  rm -rf ./magento2-2.1.2/
  cd ${nginx_root}
  php /usr/local/bin/composer install
else
  cd ${nginx_root}
  php /usr/local/bin/composer install
fi

chown www-data:www-data -R ${nginx_root}
cd ${nginx_root} && find var vendor pub/static pub/media app/etc -type f -exec chmod g+w {} \; && find var vendor pub/static pub/media app/etc -type d -exec $

if [ ! -f ${nginx_root}/app/etc/env.php ]; then
  echo -e "Magento is not install, going to install now"
  cd ${nginx_root}
  php bin/magento setup:install --base-url=${nginx_host}/ \
    --db-host=magento2-mysql --db-name=magento2 --db-user=magento2 --db-password=magento2_pass \
    --admin-firstname=Magento --admin-lastname=Admin --admin-email=admin@magento.com \
    --admin-user=admin --admin-password=admin123 --language=en_US \
    --currency=USD --timezone=America/Chicago --use-rewrites=1
  php bin/magento setup:config:set --backend-frontname="admin"
fi

php bin/magento setup:store-config:set --base-url="${nginx_host}/"
php bin/magento setup:store-config:set --base-url-secure="${nginx_host}/"

php /usr/local/bin/composer require ahmadfarzan/magento2-shopfinder
php /usr/local/bin/composer update

rm -rf var/cache var/page_cache var/view_preprocessed pub/static
mkdir var/cache var/page_cache var/view_preprocessed pub/static
php bin/magento setup:upgrade
php bin/magento cache:clean
php bin/magento setup:static-content:deploy

chown www-data:www-data -R ${nginx_root}
cd ${nginx_root} && find var vendor pub/static pub/media app/etc -type f -exec chmod g+w {} \; && find var vendor pub/static pub/media app/etc -type d -exec chmod g+w {} \; && chmod u+x bin/magento && rm -rf var/cache var/page_cache
chmod 777 -R ${nginx_root}/var/

echo "> Starting Nginx..."
service nginx start

echo "> Reading PHP7 errors log (/var/log/php7.0-fpm.log)... & Reading Nginx errors log (/var/log/nginx/error.log)"
tail -f /var/log/php7.0-fpm.log -f /var/log/nginx/error.log
